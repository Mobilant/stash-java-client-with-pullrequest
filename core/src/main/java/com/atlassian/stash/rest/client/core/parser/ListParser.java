package com.atlassian.stash.rest.client.core.parser;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.gson.JsonElement;

import java.util.Collections;
import java.util.List;

class ListParser<T> implements Function<JsonElement, List<T>> {
    private final Function<JsonElement, T> elementParser;

    public ListParser(Function<JsonElement, T> elementParser) {
        this.elementParser = elementParser;
    }

    @Override
    public List<T> apply(final JsonElement json) {
        if (json == null) {
            return Collections.emptyList();
        } else {
            return ImmutableList.copyOf(Iterables.transform(json.getAsJsonArray(), elementParser));
        }
    }
}
