package com.atlassian.stash.rest.client.api.entity;

import com.google.common.base.Objects;

import javax.annotation.Nullable;

/**
 * Describes a project
 *
 * <p/>
 * Project is a group of repositories with common settings eg. permissions
 */
public class Project {
    private final String key;
    private final long id;
    private final String name;
    @Nullable
    private final String description;
    private final boolean isPublic;
    private final boolean isPersonal;
    private final String type;
    private final String selfUrl;

    public Project(final String key, final long id, final String name, @Nullable final String description,
                   final boolean isPublic, boolean isPersonal, final String type, final String selfUrl) {
        this.key = key;
        this.id = id;
        this.name = name;
        this.description = description;
        this.isPublic = isPublic;
        this.isPersonal = isPersonal;
        this.type = type;
        this.selfUrl = selfUrl;
    }

    /**
     * @return the project's unique key
     */
    public String getKey() {
        return key;
    }

    /**
     * @return the project's ID, which represents its primary key.
     */
    public long getId() {
        return id;
    }

    /**
     * @return the project's name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the project's description
     */
    @Nullable
    public String getDescription() {
        return description;
    }

    /**
     * Retrieves a flag indicating whether this project is public.
     * <p/>
     * Note, this flag is taken into account when calculating whether this project is accessible to
     * unauthenticated users but is <i>not</i> the definitive answer.
     *
     * @return {@code true} if the project has been marked as public, {@code false} otherwise
     */
    public boolean isPublic() {
        return isPublic;
    }

    /**
     * Retrieves a flag indicating whether this project is a Stash user's personal project in the system.
     *
     * @return {@code true} if the project is a personal project, {@code false} otherwise
     */
    public boolean isPersonal() {
        return isPersonal;
    }

    /**
     * Retrieves the project's type. As of writing it may be NORMAL or PERSONAL
     *
     * @return the type for this project
     */
    public String getType() {
        return type;
    }

    /**
     * Retrieves the project's url ie. url leading to Stash application project page
     * @return the url for this project
     */
    public String getSelfUrl() {
        return selfUrl;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("key", key)
                .add("id", id)
                .add("name", name)
                .add("description", description)
                .add("public", isPublic)
                .add("personal", isPersonal)
                .add("type", type)
                .add("selfUrl", selfUrl)
                .toString();
    }
}
