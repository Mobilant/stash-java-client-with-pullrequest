package com.atlassian.stash.rest.client.api;

import com.atlassian.stash.rest.client.api.entity.Branch;
import com.atlassian.stash.rest.client.api.entity.Page;
import com.atlassian.stash.rest.client.api.entity.Permission;
import com.atlassian.stash.rest.client.api.entity.Project;
import com.atlassian.stash.rest.client.api.entity.Repository;
import com.atlassian.stash.rest.client.api.entity.RepositorySshKey;
import com.atlassian.stash.rest.client.api.entity.UserSshKey;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Map;

public interface StashClient {
    /**
     * Get a list of all projects the user can see.
     *
     * @param start           index of the first result to retrieve (for paging)
     * @param limit           total number of results to retrieve after start (for Paging)
     * @return A page of Stash projects
     *
     * @throws StashException
     * @throws StashRestException
     */
    @Nonnull
    Page<Project> getAccessibleProjects(long start, long limit);


    /**
     * Search for repositories the user can see.  Repositories contain project information.
     *
     * @param projectKey     optional. Filter by an individual project
     * @param query           Search term to filter repositories by
     * @param start           index of the first result to retrieve (for paging)
     * @param limit           total number of results to retrieve after start (for Paging)
     * @return A page of Stash repositories
     *
     * @throws StashException
     * @throws StashRestException
     */
    @Nonnull
    public Page<Repository> getRepositories(@Nullable String projectKey, @Nullable String query,
                                                      long start, long limit);


    /**
     * Retrieve a specific repository object
     *
     * @param projectKey      of the project the repo is in
     * @param repositorySlug  slugified version of the repo name
     * @return A Stash repository or null if not found
     *
     * @throws StashException
     * @throws StashRestException
     */
    @Nullable
    public Repository getRepository(@Nonnull String projectKey, @Nonnull String repositorySlug);


    /**
     * Search for the branches for a specific repository
     *
     * @param projectKey      the repository belongs to
     * @param repositorySlug  repository (this is the slugified version of the repository name, not the raw repository name)
     * @param query           search term for branch name
     * @param start           index of the first result to retrieve (for paging)
     * @param limit           total number of results to retrieve after start (for Paging)
     * @return A page of the repository branches
     *
     * @throws StashException
     * @throws StashRestException
     */
    @Nonnull
    public Page<Branch> getRepositoryBranches(@Nonnull String projectKey, @Nonnull String repositorySlug,
                                                        @Nullable String query, long start, long limit);

    /**
     * Retrieves default branch for a specific repository
     * @param projectKey        the repository belongs to
     * @param repositorySlug    repository (this is the slugified version of the repository name, not the raw repository name)
     * @return repository branch
     */
    @Nullable
    public Branch getRepositoryDefaultBranch(@Nonnull final String projectKey, @Nonnull final String repositorySlug);


    /**
     * Retrieves access keys (SSH public keys) for given repository
     *
     * @param projectKey     project key
     * @param repositorySlug  repository key
     * @param start           index of the first result to retrieve (for paging)
     * @param limit           total number of results to retrieve after start (for Paging)
     * @return A page of repository access keys
     *
     * @throws StashException
     * @throws StashRestException
     */
    @Nonnull
    Page<RepositorySshKey> getRepositoryKeys(@Nonnull String projectKey, @Nonnull String repositorySlug,
                                                       long start, long limit);

    /**
     * Adds to the repository a new access key (SSH public key) with given permission
     *
     * @param projectKey     project key
     * @param repositorySlug  repository key
     * @param publicKey       access key value
     * @param keyLabel        label stored with access key
     * @param keyPermission   type of permission given by public key
     *
     * @return true on success
     *
     * @throws StashException
     * @throws StashRestException
     * @throws StashUnauthorizedRestException
     */
    boolean addRepositoryKey(@Nonnull String projectKey, @Nonnull String repositorySlug, @Nonnull String publicKey,
                             @Nullable String keyLabel, @Nonnull Permission keyPermission);

    /**
     * Checks if provided access key (SSH public key) is already registered as a repository key on Stash server
     *
     * @param projectKey     project key
     * @param repositorySlug  repository key
     * @param publicKey Access key (SSH public key) value
     * @return true if key is already available on server, false if no key is provided or provided key is not registered as repository key on server
     *
     * @throws StashException
     * @throws StashRestException
     */
    boolean isRepositoryKey(@Nonnull String projectKey, @Nonnull String repositorySlug, @Nonnull String publicKey);

    /**
     * Retrieves access keys (SSH public keys) for current user
     *
     * @param start index of the first result to retrieve (for paging)
     * @param limit total number of results to retrieve after start (for Paging)
     * @return A page of user access keys
     *
     * @throws StashException
     * @throws StashRestException
     */
    @Nonnull
    Page<UserSshKey> getCurrentUserKeys(long start, long limit);

    /**
     * Checks if provided access key (SSH public key) is already registered as a user key on Stash server
     *
     * @param publicKey Access key (SSH public key) value
     * @return true if key is already available on server, false if no key is provided or provided key is not registered as user key on server
     *
     * @throws StashException
     * @throws StashRestException
     */
    boolean isUserKey(@Nonnull String publicKey);

    /**
     * Adds to current user a new access key (SSH public key)
     *
     * @param publicKey       access key value
     * @param keyLabel        label stored with access key
     *
     * @return true on success
     *
     * @throws StashException
     * @throws StashRestException
     */
    boolean addUserKey(@Nonnull String publicKey, @Nullable String keyLabel);

    /**
     * Retrieves stash application properties like version, buildNumber or buildDate
     *
     * @return A map of Stash application properties
     *
     * @throws StashException
     * @throws StashRestException
     */
    @Nonnull
    Map<String, String> getStashApplicationProperties();
}
